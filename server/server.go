// Package main implements a server for Calculator service.
package main

import (
	"context"
	"google.golang.org/grpc"
	"log"
	"net"

	pb "condaatje.io/apis/calculator"
)

const (
	port = ":5051"
)

// server is used to implement helloworld.GreeterServer.
type server struct {
	pb.UnimplementedCalculatorServer
}

func (s *server) Increment(ctx context.Context, in *pb.Number) (*pb.Number, error) {
	log.Printf("Received: %v", in.GetN())
	return &pb.Number{N: 1 + in.GetN()}, nil
}

func (s *server) Decrement(ctx context.Context, in *pb.Number) (*pb.Number, error) {
	log.Printf("Received: %v", in.GetN())
	return &pb.Number{N: in.GetN() - 1}, nil
}

func (s *server) CountFromTo(tpl *pb.Tuple, stream pb.Calculator_CountFromToServer) error {
	for i := tpl.GetX(); i <= tpl.GetY(); i++ {
		if err := stream.Send(&pb.Number{N: i}); err != nil {
			return err
		}
	}
	return nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterCalculatorServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
