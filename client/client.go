package main

import (
	"context"
	"google.golang.org/grpc"
	"io"
	"log"
	"os"
	"strconv"
	"time"

	pb "condaatje.io/apis/calculator"
)

const (
	address    = "localhost:5051"
	defaultNum = int32(42)
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewCalculatorClient(conn)

	// Contact the server and print out its response.
	num := defaultNum
	if len(os.Args) > 1 {
		numx, _ := strconv.Atoi(os.Args[1])
		num = int32(numx)
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Increment(ctx, &pb.Number{N: num})
	if err != nil {
		log.Fatalf("could not increment: %v", err)
	}
	log.Printf("Incremented: %v", r.GetN())

	r, err = c.Decrement(ctx, &pb.Number{N: num})
	if err != nil {
		log.Fatalf("could not Decrement: %v", err)
	}
	log.Printf("Decremented: %v", r.GetN())

	printCounting(c, &pb.Tuple{X: 0, Y: num})

}

func printCounting(client pb.CalculatorClient, tpl *pb.Tuple) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	stream, err := client.CountFromTo(ctx, tpl)
	if err != nil {
		log.Fatalf("%v.CountFromTo(_) = _, %v", client, err)
	}
	for {
		n, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("%v.CountFromTo(_) = _, %v", client, err)
		}
		log.Printf("count: %v", n.GetN())
	}
}
