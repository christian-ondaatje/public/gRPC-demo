# Demo gRPC API
### Christian Ondaatje

- [ ] read gRPC documentation
- [ ] read protobuf documentation
- [ ] create example apiclient
- [ ] create example apiserver
- [ ] run example workflow


# dev

1. edit protobuf in `demo.proto`
2. `protoc --proto_path=. --go_out=. --go-grpc_out=. ./demo.proto`




https://grpc.io/docs/languages/go/quickstart/
good example code here: https://github.com/grpc/grpc-go


go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
go get google.golang.org/grpc
