module condaatje.io-demo-api

go 1.15

require (
	condaatje.io/apis/calculator v0.0.0-00010101000000-000000000000
	github.com/golang/protobuf v1.4.3 // indirect
	google.golang.org/grpc v1.35.0
	google.golang.org/grpc/examples v0.0.0-20210218181225-26c143bd5f59 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)

replace condaatje.io/apis/calculator => ./condaatje.io/apis/calculator
